This is a CSS/SASS-based grid system for use in templates.

It allows you to lay out content in rows and columns which automatically align between parent and child containers. It was built to facilitate the creation of rich print-style and tiled layouts without the need to build grids from scratch or get lost in rabbit holes of alignments and padding. 

Existing (and widely) used grid frameworks are good at simple column based layout but are less than simple when attempting to combine flex columns, tiled layouts, auto placement of tiles, and aligning everything to underlying visual grids. They are also opinionated in visual styling. 

They also require SASS compilation to create static CSS when you want to divert from defaults. It would be nice to be able to just reconfigire the CSS at run time via simple CSS or JS and not fight with underlying CSS on colours and such. 

For this reason, where it is supported, core variables are exposed as CSS custom properties and can be changed on the fly without recompilation, and any underlying visual styles are optional.


The main features currently are:

- Structure is inherited from parents to children. Eg: If you set a container to have six columns and place a three column child within it, the child will have three columns and be 50% width, and a three column item within the child will automaticaly span to 100%. In this way items automatically align against a master column layout. The underlying layouts that align items only change when you declare them to. 

- Containers can be explicitly flex or grid based. Column counts and layouts will work the same in both, and each has graceful fallbacks for older browsers (currently servicable to ie11). 

- SASS variables are exposed as CSS custom properties (CSS variables) so you can re-declare them in CSS or JS without the need to recompile SASS. You can also use regular CSS conditionals (like @supports and breakpoints) to dynamically change the core layout variables and layout. 

- Breakpoints (and the quantity and names of breakpoints) are fully customisable

- Column widths can be set via HTML classes by breakpoint

- Items can be made square (across fluid widths) via a makeSquare class

- All CSS class names can be prefixed via a single SASS variable to avoid collision with legacy styles

- Grids leverage CSS grid layout to automatically tile items 

- Classes can be inherited via @extend. So you can create a teaser that automatically runs two columns wide and two rows deep and is square in a layout at a given breakpoint simply by creating a media query and @extending those three classes in SASS. 